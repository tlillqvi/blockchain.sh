# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Learning purposes

### How do I get set up? ###

Example output:

    $ ./blockchain.sh 
    Mining .
    Block: 0
    Prev: none
    Transactions:[]
    Signature: 000903c76ccc03757e0c70873c398771d03b4e511795b21626859b073cfe0c8b Nonce: 10
    Enter transaction:A->B 1 coin
    Enter transaction:X->Y 1 coin
    Enter transaction:<enter>
    Mining .
    Block: 1
    Prev: 000903c76ccc03757e0c70873c398771d03b4e511795b21626859b073cfe0c8b
    Transactions:[A->B 1 coin,X->Y 1 coin]
    Signature: 00e2d34a8c5c265fa08bce7b50225134ded0f58f5f82ba0ba86dd60ea6f6ab8b Nonce: 70
    Enter transaction:B->Y 2 coins<enter>
    Enter transaction:<enter>
    Mining .
    Block: 2
    Prev: 00e2d34a8c5c265fa08bce7b50225134ded0f58f5f82ba0ba86dd60ea6f6ab8b
    Transactions:[B->Y 2 coins]
    Signature: 004a73c86a969bda2e301b9cdd2b35a9324c691aab24948fb856712fc35d0ffa Nonce: 443
    Enter transaction:<enter>
    No transactions entered. Exiting!
    
    Full chain:
    
    -----
    Block: 0
    Prev: none
    Transactions:[]
    Signature: 000903c76ccc03757e0c70873c398771d03b4e511795b21626859b073cfe0c8b Nonce: 10
    -----
    Block: 1
    Prev: 000903c76ccc03757e0c70873c398771d03b4e511795b21626859b073cfe0c8b
    Transactions:[A->B 1 coin,X->Y 1 coin]
    Signature: 00e2d34a8c5c265fa08bce7b50225134ded0f58f5f82ba0ba86dd60ea6f6ab8b Nonce: 70
    -----
    Block: 2
    Prev: 00e2d34a8c5c265fa08bce7b50225134ded0f58f5f82ba0ba86dd60ea6f6ab8b
    Transactions:[B->Y 2 coins]
    Signature: 004a73c86a969bda2e301b9cdd2b35a9324c691aab24948fb856712fc35d0ffa Nonce: 443
    -----


### Who do I talk to? ###

* Repo owner or admin
