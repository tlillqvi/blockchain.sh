#!/bin/bash

set -eu

# Print the full chain on exit
fullchain=""
trap 'echo -e "\nFull chain:\n$fullchain\n-----"' EXIT

mine_block()
{
    block="$1"
    echo >&2 -n "Mining ."
    local miningnonce=0
    while true; do
        miningnonce=$(($miningnonce+1));

        test $(($miningnonce%1000)) -eq 0 && echo >&2 -n "."

        local sig=$(echo "$block$miningnonce" | sha256sum | awk '{print $1}');
        # Difficulty set to 2 leadin zeroes
        test "${sig:0:2}" = "00" && {
            echo "$sig $miningnonce"
            echo >&2
            return 0
        }
    done
}

read_transactions() {
    local transactions="["
    local transaction=""
    while true; do
        read -p "Enter transaction:" transaction;

        test -z "$transaction" && {
            echo "$transactions]"
            return 0
        }

        test "$transactions" = "[" &&
        transactions="$transactions$transaction" ||
        transactions="$transactions,$transaction"
    done
}

print_block() {
    local block="$1"
    local sig="$2"
    local nonce="$3"
    local OUTPUT="\
$block
Signature: $sig Nonce: $nonce"
    echo "$OUTPUT"
}

blockchain()
{
    # Mine the genesis block
    local blocknum=0
    local sig=none
    local block="$(echo -e "Block: $blocknum\nPrev: $sig\nTransactions:[]")"
    local sig_nonce=$(mine_block "$block")
    local sig="$(set -- $sig_nonce; echo $1)"
    local nonce="$(set -- $sig_nonce; echo $2)"
    local OUTPUT="$(print_block "$block" "$sig" "$nonce")"

    echo "$OUTPUT"

    fullchain="\
$fullchain
-----
$OUTPUT"

    while true; do
        blocknum=$(($blocknum+1))

        local transactions="$(read_transactions)"
        test "$transactions" = "[]" && {
            echo "No transactions entered. Exiting!"
            exit
        }

        local block="$(echo -e "Block: $blocknum\nPrev: $sig\nTransactions:$transactions")";
        local sig_nonce=$(mine_block "$block")
        sig="$(set -- $sig_nonce; echo $1)"
        local nonce="$(set -- $sig_nonce; echo $2)"
        local OUTPUT="$(print_block "$block" "$sig" "$nonce")"

        test -n "$OUTPUT" || { echo "FAIL!!"; exit 1; }

        echo "$OUTPUT"
        fullchain="\
$fullchain
-----
$OUTPUT"
    done
}

blockchain
